# 2018 UC SEP team 12 project - "A Sudoku Solver"

## App Overview

1. Android SDK 28+
2. Third party libraries: OpenCV 3.4.2, Tess-two 9.0.0
3. Release versions [here](https://drive.google.com/open?id=1an--gduQ_8TO-tV1TwDurOksdcxghm3x)
4. Permissions required: READ_EXTERNAL_STORAGE/WRITE_EXTERNAL_STORAGE/Camera
5. APK size: around 80M (including the OpenCV libraries,supporting arm/x86/mips)

---

## How does the app work?

Basically, the app works in the following ways:

1. Get an image from ***Camera*** or ***Gallery***
2. ***Crop*** the image properly
3. Transfer the image to ***Bitmap***
4. Use the OpenCV functions to ***threshold*** the image
5. Try to find ***the biggest quadrilateral*** in the image
6. ***Transform*** the quadrilateral area to a normal Square, say, ***the normalized bitmap***
7. Process the normalized bitmap,via openCV ***adaptative thresholding/findContours***
8. Split the normalized bitmap to *** 9*9 grids ***
9. Use ***Tesseract API*** to parse each grids, finally we get all the numbers in the image
10. Use ***brute force***/***heuristic algorithms*** to solve the Sudoku puzzle.

---

## How to use the app?

Based on v2.0


### 1. The front page
    
   ![pic1](resource/v2.0/v2_1_home_page.png)
	
### 2. Take a photo using camera and solve it

|  |  |  |
| ------ | ------ | ------ |
| 1)tap the "from camera" button | 2)take a picture | 3)confirm the photo  |
| ![pic1](resource/v2.0/v2_1_home_page.png) | ![pic1](resource/v2.0/v2_2_2_open_camera.png) | ![pic1](resource/v2.0/v2_2_3_confirm_photo.png) |
| 4) crop the image | 5) back to home page, with the final image | 6) tap the process button  |
| ![pic1](resource/v2.0/v2_2_4_crop_image.png) | ![pic1](resource/v2.0/v2_2_5_back_to_home_page.png) | ![pic1](resource/v2.0/v2_2_6_go_to_parse.png) |
| 7) tap the parse button | 8) show all the parse numbers | 9) tap the "solve all" button, show all the answers  |
| ![pic1](resource/v2.0/v2_2_7_click_pase_button.png) | ![pic1](resource/v2.0/v2_2_8_show_parse_result.png) | ![pic1](resource/v2.0/v2_2_9_solve_all.png) |
| 10) tap the parse button to reset the puzzle | 11) tap the hint button, shows all the number 2 answers | 12) tap the hint button again, shows all the number 3 answers  |
| ![pic1](resource/v2.0/v2_2_10_reset_puzzle.png) | ![pic1](resource/v2.0/v2_2_11_hint1.png) | ![pic1](resource/v2.0/v2_2_12_hint2.png) |
| 13) tap the hint button, shows all the number 4 answers | 14) tap the hint button, shows all the number 5 answers | 15) tap the hint button again, shows all the number 6 answers  |
| ![pic1](resource/v2.0/v2_2_13_hint3.png) | ![pic1](resource/v2.0/v2_2_14_hint4.png) | ![pic1](resource/v2.0/v2_2_15_hint5.png) |
| 16) tap the hint button, shows all the number 7 answers | 17) tap the hint button, shows all the number 8 answers | 18) tap the hint button again, shows all the number 9 answers  |
| ![pic1](resource/v2.0/v2_2_16_hint6.png) | ![pic1](resource/v2.0/v2_2_17_hint7.png) | ![pic1](resource/v2.0/v2_2_18_hint8.png) |


### 3. Select an image from Gallery ans solve it

|  |  |  |
| ------ | ------ | ------ |
| 1) tap the "from gallery" button | 2) select a picture | 3) crop the image  |
| ![pic1](resource/v2.0/v2_1_home_page.png) | ![pic1](resource/v2.0/v2_3_2_select_image.png) | ![pic1](resource/v2.0/v2_3_3_crop_image.png) |
| 4) back to home page, with the final image | 5) tap the process button | 6) show all the parse numbers  |
| ![pic1](resource/v2.0/v2_3_4_back_to_home.png) | ![pic1](resource/v2.0/v2_3_5_to_parse_page.png) | ![pic1](resource/v2.0/v2_3_6_show_parse_numbers.png) |
| 7) tap the "solve all" button, show all the answers | 8) tap the parse button to reset the puzzle | 9) tap the hint button, shows all the number 1 answers  |
| ![pic1](resource/v2.0/v2_3_7_show_all_solutions.png) | ![pic1](resource/v2.0/v2_3_8_hint1.png) | ![pic1](resource/v2.0/v2_3_9_hint2.png) |
| 10) tap the hint button, shows all the number 2 answers | 11) tap the hint button, shows all the number 3 answers | 12) tap the hint button, shows all the number 4 answers  |
| ![pic1](resource/v2.0/v2_3_10_hint3.png) | ![pic1](resource/v2.0/v2_3_11_hint11.png) | ![pic1](resource/v2.0/v2_3_12_hint12.png) |

---


## Reference

https://github.com/rmtheis/tess-two

https://stackoverflow.com/questions/14992637/ocr-tesseract-set-up-only-numbers

https://medium.com/@sukritipaul005/a-beginners-guide-to-installing-opencv-android-in-android-studio-ea46a7b4f2d3

---

## END
