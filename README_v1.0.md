# 2018 UC SEP team 0-SudokuSolver

## App Overview

1. Android SDK 28+
2. Third party libraries: OpenCV 3.4.2, Tess-two 9.0.0
3. Release versions [here](https://drive.google.com/open?id=1an--gduQ_8TO-tV1TwDurOksdcxghm3x)
4. Permissions required: READ_EXTERNAL_STORAGE/WRITE_EXTERNAL_STORAGE/Camera
5. APK size: around 80M (including the OpenCV libraries,supporting arm/x86/mips)

---

## How does the app work?

Basically, the app works in the following ways:

1. Get an image from camera or gallery
2. Transfer the image to ***Bitmap***
3. Use the OpenCV functions to ***threshold*** the image
4. Try to find ***the biggest quadrilateral*** in the image
5. ***Transform*** the quadrilateral area to a normal Square, say, ***the normalized bitmap***
6. Process the normalized bitmap,via openCV ***adaptative thresholding/findContours***
7. Split the normalized bitmap to *** 9*9 grids ***
8. Use ***Tesseract API*** to parse each grids, finally we get all the numbers in the image
9. Use brute force/heuristic algorithms to solve the Sudoku puzzle.

---

## Examples

Based on v1.0

1. The front page
>    ![pic1](resource/v1_20180922215253.jpg)
2. Parse the numbers in the image
>    ![pic1](resource/v2_20180922215301.png)
3. Solve the puzzle in one time
>    ![pic1](resource/v3_20180922215310.png)

---

## END
