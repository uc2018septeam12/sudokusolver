package com.team0.sep.sudokusolver;

import android.Manifest;
import android.content.ContentValues;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class ImageOptionActivity extends AppCompatActivity {

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 998;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 997;
    public static final int MY_PERMISSIONS_REQUEST_CAMERA = 996;

    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_SELECT = 2;
    static final int REQUEST_CROP_IMAGE = 3;

    ImageView imageView;
    Button btnProcessImage;

    Uri imageUri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_option);

        checkPermissions();

        imageView = findViewById(R.id.imageView);

        Button btnTakePicture = findViewById(R.id.btnToCamera);
        btnTakePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchTakePictureIntent();
            }
        });

        Button btnSelectPicture = findViewById(R.id.btnToGallery);
        btnSelectPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dispatchSelectPictureIntent();

            }
        });

        btnProcessImage = findViewById(R.id.btnProcess);
        btnProcessImage.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //dispatchSelectPictureIntent();
                sendImageToSudokuSolver();
            }
        });
        btnProcessImage.setEnabled(false);

    }

    /**
     * send the current image to next activity
     */
    public void sendImageToSudokuSolver() {

        Intent intent = new Intent(this, SolveSudokuActivity.class);
        intent.putExtra("OriginalImageUri", imageUri.toString());

        startActivity(intent);
    }

    /**
     * to another activity
     * select the local photo from Gallery
     */
    public void dispatchSelectPictureIntent() {

        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Audio.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(intent.createChooser(intent, "Select Image"), REQUEST_IMAGE_SELECT);

    }

    /**
     * take picture using the camera
     */
    public void dispatchTakePictureIntent() {

        ContentValues values = new ContentValues();
        values.put(MediaStore.Images.Media.TITLE, "New Picture");
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera");
        imageUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);

        Log.i("image uri", imageUri.toString());

        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageUri);
        startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);

    }

    /**
     * Crop the image
     * using the android crop function
     * @param orginalImageUri the uri of the selected image or new photo
     */
    public void dispatchCropImageIntent(Uri orginalImageUri){

        /**
         * https://blog.csdn.net/superxlcr/article/details/70209774
         */
        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(orginalImageUri, "image/*");
        //intent.putExtra("crop", "true");
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);
        intent.putExtra("outputX", 1024);
        intent.putExtra("outputY", 1024);
        intent.putExtra("scale", true);
        intent.putExtra("circleCrop", false);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, orginalImageUri);
        intent.putExtra("return-data", false);
        intent.putExtra("outputFormat", Bitmap.CompressFormat.JPEG.toString());
        intent.putExtra("noFaceDetection", true); // no face detection
        intent = Intent.createChooser(intent, "Crop Image");
        startActivityForResult(intent, REQUEST_CROP_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            dispatchCropImageIntent(imageUri);

//            imageView.setImageURI(imageUri);
            Log.i("Image capture>>>", "success!!!");
            btnProcessImage.setEnabled(true);
        }
        if (requestCode == REQUEST_IMAGE_SELECT && resultCode == RESULT_OK) {

            Uri selectedImageUri = data.getData();
//            imageView.setImageURI(selectedImageUri);
            imageUri = selectedImageUri;
            Log.i("Image select>>>", "success!!!");

            dispatchCropImageIntent(selectedImageUri);
            btnProcessImage.setEnabled(true);
        }
        if(requestCode == REQUEST_CROP_IMAGE && resultCode == RESULT_OK){
            //show the crop/selected image
            imageView.setImageURI(imageUri);
        }

    }

    /**
     * Ask for permissions
     */
    private void checkPermissions() {
        /**
         * check permission
         */
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.CAMERA},
                    MY_PERMISSIONS_REQUEST_CAMERA);
        }
    }



}
