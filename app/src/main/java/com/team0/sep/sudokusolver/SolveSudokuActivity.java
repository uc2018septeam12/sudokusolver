package com.team0.sep.sudokusolver;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.team0.sep.sudokusolver.core.SudokuHintSolver;
import com.team0.sep.sudokusolver.core.SudokuRecognizer;
import com.team0.sep.sudokusolver.core.SudokuSolver2;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * The sloving activity
 * functions:
 *     1. parse image numbers
 *     2. solve the puzzle in one step
 *     3. solve the puzzle in multiple steps, along with hints
 */
public class SolveSudokuActivity extends AppCompatActivity {

    private static final String TAG = "SolveSudokuActivity Tag";

    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 998;
    public static final int MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 997;

    public final static String TRAINED_DATA_FOLDER = "/Sudoku_tesseract";
    public final static String TRAINED_DATA_NAME = "num.traineddata";

    public SudokuRecognizer sudokuRecognizer;

    Bitmap bitmapOriginal;
    Bitmap bitmapResult;
    ImageView originalImageView;
    ImageView resultImageView;
//    EditText editText;

    int[][] sudokuNumMatrix = new int[9][9];
    int[][] sudokuHintMatrix;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //check permissions
        checkPermissions();

        //check ocr train data
        checkTrainingData();

        //initialize the OpenCV/OCR api
        sudokuRecognizer = new SudokuRecognizer();

        //process the bitmap from previous activity
        Intent intent = getIntent();
        Bundle extras =intent.getExtras();

        if(extras.getString("OriginalImageUri")!=null){
            Uri imageUri = imageUri = Uri.parse(extras.getString("OriginalImageUri"));
            try {
                bitmapOriginal = MediaStore.Images.Media.getBitmap(getContentResolver(), imageUri);
            } catch (IOException e) {
                Log.e("Image error","parse URI error");
                bitmapOriginal = BitmapFactory.decodeResource(getResources(), R.drawable.sudoku1);
            }
        }else{
            bitmapOriginal = BitmapFactory.decodeResource(getResources(), R.drawable.sudoku1);
        }

        bitmapResult =BitmapFactory.decodeResource(getResources(), R.drawable.sudokublank2);

        originalImageView = findViewById(R.id.originalImageView);
        originalImageView.setImageBitmap(bitmapOriginal);
        resultImageView = findViewById(R.id.resultImageView);
        resultImageView.setImageBitmap(bitmapResult);
//        editText = findViewById(R.id.numberText);

        Button btnParse = findViewById(R.id.btnProcess);
        btnParse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sudokuRecognizer.setOriginalBitmap(bitmapOriginal);
                sudokuNumMatrix = sudokuRecognizer.processSudokuImage();

                String resultStr = "";
                for (int i = 0; i < 9; i++) {
                    for (int j = 0; j < 9; j++) {
                        resultStr += sudokuNumMatrix[i][j];
                    }
                    Log.i("::match result::", resultStr);
                    resultStr += "\n";
                }
//                editText.setText(resultStr);

                resultImageView.setImageBitmap(
                        getPuzzleBitmap(sudokuNumMatrix));
            }
        });

        Button btnSolve = findViewById(R.id.btnSolveAll);
        btnSolve.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                //create a deep copy
                int [][] sodukuResultMatrix = new int[9][9];
                for(int i = 0;i<9;i++){
                    for(int j = 0;j<9;j++){
                        sodukuResultMatrix[i][j] = sudokuNumMatrix[i][j];
                    }
                }
                SudokuSolver2.solveSudoku(sodukuResultMatrix);
                resultImageView.setImageBitmap(
                        getResultSudokuBitmap(sudokuNumMatrix,sodukuResultMatrix));
            }
        });

        Button btnHint = findViewById(R.id.btnHint);
        btnHint.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                //
                int [][] sodukuAllResultMatrix = new int[9][9];
                for(int i = 0;i<9;i++){
                    for(int j = 0;j<9;j++){
                        sodukuAllResultMatrix[i][j] = sudokuNumMatrix[i][j];
                    }
                }

                //init the hint matrix
                if(sudokuHintMatrix==null){
                    sudokuHintMatrix = new int[9][9];
                    for(int i = 0;i<9;i++){
                        for(int j = 0;j<9;j++){
                            sudokuHintMatrix[i][j] = sudokuNumMatrix[i][j];
                        }
                    }
                }

                SudokuSolver2.solveSudoku(sodukuAllResultMatrix);

                int [][] sodukuResultMatrix = SudokuHintSolver.solveStepByStep(sudokuHintMatrix,sodukuAllResultMatrix);

                resultImageView.setImageBitmap(
                        getResultSudokuBitmap(sudokuNumMatrix,sodukuResultMatrix));

                //for next step
                sudokuHintMatrix = sodukuResultMatrix;
            }
        });

    }

    /**
     * return a bitmap that contains all the numbers
     * @return
     */
    public Bitmap getPuzzleBitmap(int [][] sudokuOriMatrix){
        Bitmap tempMap = bitmapResult.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(tempMap);
        Paint textPaint=new Paint(Paint.ANTI_ALIAS_FLAG|Paint.DEV_KERN_TEXT_FLAG);
        textPaint.setAntiAlias(true);
        textPaint.setDither(true);
        textPaint.setColor(Color.RED);
        textPaint.setTextSize(120);
        textPaint.setTypeface(Typeface.DEFAULT_BOLD);
        textPaint.setTextAlign(Paint.Align.LEFT);

        for(int iCol = 0;iCol<9;iCol++){ //col
            for(int iRow = 0;iRow<9;iRow++){ //row
                float x = (tempMap.getWidth() / 18)*(2*iCol+1)-20.0f;
                float y = (tempMap.getHeight() / 9)*(iRow+1)-7.0f;

                int num = sudokuOriMatrix[iRow][iCol];

                if(sudokuOriMatrix[iRow][iCol]!=0){
                    // original numbers,blue
                    textPaint.setColor(Color.BLUE);
                    canvas.drawText(Integer.toString(num),x,y,textPaint);
                }
            }
        }
        return tempMap;
    }

    /**
     * return a bitmap that contains all the numbers
     * @return
     */
    public Bitmap getResultSudokuBitmap(int [][] sudokuOriMatrix, int [][] sodukuResultMatrix){
        Bitmap tempMap = bitmapResult.copy(Bitmap.Config.ARGB_8888, true);
        Canvas canvas = new Canvas(tempMap);
        Paint textPaint=new Paint(Paint.ANTI_ALIAS_FLAG|Paint.DEV_KERN_TEXT_FLAG);
        textPaint.setAntiAlias(true);
        textPaint.setDither(true);
        textPaint.setColor(Color.RED);
        textPaint.setTextSize(120);
        textPaint.setTypeface(Typeface.DEFAULT_BOLD);
        textPaint.setTextAlign(Paint.Align.LEFT);
        //canvas.drawText("6",(tempMap.getWidth() / 9),(tempMap.getHeight() / 9),textPaint);
//                canvas.drawText("6",20.0f,20.0f,textPaint);
        for(int iCol = 0;iCol<9;iCol++){ //col
            for(int iRow = 0;iRow<9;iRow++){ //row
                float x = (tempMap.getWidth() / 18)*(2*iCol+1)-20.0f;
                float y = (tempMap.getHeight() / 9)*(iRow+1)-7.0f;

                int num = sodukuResultMatrix[iRow][iCol];

                if(sudokuOriMatrix[iRow][iCol]==0){
                    // solution numbers,red
                    textPaint.setColor(Color.RED);
                }else{
                    // original numbers,blue
                    textPaint.setColor(Color.BLUE);
                }

                if(num!=0) {
                    canvas.drawText(Integer.toString(num), x, y, textPaint);
                }

            }
        }
        return tempMap;
    }

    public void checkTrainingData(){
        /**
         * if the training data is not existed, copy it to
         * the phone storage
         */
        if(!hasTrainingData()){
            copyTheTrainingData();
        }
    }

    public boolean hasTrainingData(){

        String dataPath = Environment.getExternalStorageDirectory() + TRAINED_DATA_FOLDER;
        String strFile = dataPath+"/tessdata/"+TRAINED_DATA_NAME;
        try{
            File file=new File(strFile);
            if(!file.exists())
            {
                return false;
            }
        }catch (Exception e){
            return false;
        }
        return true;
    }

    public void copyTheTrainingData(){

        String filePath = Environment.getExternalStorageDirectory() + TRAINED_DATA_FOLDER;

        File appResourceFolder = new File(filePath);

        if(!appResourceFolder.exists()){
            appResourceFolder.mkdirs();
        }

        filePath += "/tessdata";

        File tessDataFolder = new File(filePath);
        if(!tessDataFolder.exists()){
            tessDataFolder.mkdirs();
        }

        InputStream inputStream = getResources().openRawResource(R.raw.num);
        //FileInputStream fs = new FileInputStream(is);

        File dataFile = new File(filePath+"/"+TRAINED_DATA_NAME);

        try {
            FileOutputStream fos = new FileOutputStream(dataFile);

            byte[] buffer = new byte[inputStream.available()];

            int lenght = 0;
            while ((lenght = inputStream.read(buffer)) != -1) {
                fos.write(buffer, 0, lenght);
            }
            fos.flush();
            fos.close();
            inputStream.close();
        }catch(Exception e){
            Log.e("Training data set",e.getMessage());
        }
    }

    /**
     * Ask for permissions
     */
    private void checkPermissions() {
        /**
         * check permission
         */
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
        }
        if (ContextCompat.checkSelfPermission(getApplicationContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("Permission info","READ_EXTERNAL_STORAGE permission granted successful!");

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                //return;
            }

            case MY_PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Log.i("Permission info","WRITE_EXTERNAL_STORAGE permission granted successful!");

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                //return;
            }

            // other 'case' lines to check for other
            // permissions this app might request.
        }
    }
}
