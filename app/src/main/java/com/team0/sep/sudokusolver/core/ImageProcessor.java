package com.team0.sep.sudokusolver.core;

import android.graphics.Bitmap;
import android.util.Log;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.ArrayList;
import java.util.List;

/**
 * process image, threshold/canny/...
 */
public class ImageProcessor {

    private static final String TAG = "ImageProcessor Info";

    // the normal size
    private static final int NORMAL_IMAGE_SIZE = 512 * 2;

    static {
        if (!OpenCVLoader.initDebug()) {
            Log.i(TAG, "OpenCV Loading FAIL");
        } else {
            Log.i(TAG, "OpenCV Loading OK");
        }
    }


    /**
     * Thresh Hold
     *
     * @param bitmapOriginal the bitmap
     * @return return a new bitmap
     */
    public Bitmap threshold(Bitmap bitmapOriginal) {

        Mat srcMat = new Mat();
        Mat dstMat = new Mat();
        Utils.bitmapToMat(bitmapOriginal, srcMat);

        //convert to gray
        Imgproc.cvtColor(srcMat, dstMat, Imgproc.COLOR_BGR2GRAY);

        //thresh hold
        srcMat = dstMat;
        Imgproc.threshold(srcMat, dstMat, 60, 255, Imgproc.THRESH_BINARY);
//        Imgproc.threshold(srcMat, dstMat, 30, 255, Imgproc.THRESH_OTSU);

        //canny
//        srcMat = dstMat;
//        Mat edges = new Mat(srcMat.size(),CvType.CV_8UC1);
//        Imgproc.Canny(srcMat,edges,50,200,3,false);
//        srcMat = edges;

        //HoughLines P()
//        Mat cdst = new Mat();
//        Imgproc.cvtColor(srcMat,cdst,Imgproc.COLOR_GRAY2BGR);
//        Mat lines = new Mat();


//        Imgproc.HoughLines(srcMat, lines, 1, Math.PI/180, 150);
//
//        Log.i(TAG, "lines: " + lines.size());
//        // Draw the lines
//        for (int x = 0; x < lines.rows(); x++) {
//            double rho = lines.get(x, 0)[0],
//                    theta = lines.get(x, 0)[1];
//            double a = Math.cos(theta), b = Math.sin(theta);
//            double x0 = a*rho, y0 = b*rho;
//            Point pt1 = new Point(Math.round(x0 + 1000*(-b)), Math.round(y0 + 1000*(a)));
//            Point pt2 = new Point(Math.round(x0 - 1000*(-b)), Math.round(y0 - 1000*(a)));
//            Imgproc.line(cdst, pt1, pt2, new Scalar(0, 0, 255), 3, Imgproc.LINE_AA, 0);
//        }

//        Imgproc.HoughLinesP(srcMat, lines, 1, Math.PI / 180, 50, 1000, 300);
//        Log.i(TAG, "lines: " + lines.size());
//        // Draw the lines
//        for (int x = 0; x < lines.rows(); x++) {
//            double[] l = lines.get(x, 0);
//            Imgproc.line(cdst, new Point(l[0], l[1]), new Point(l[2], l[3]),
//                    new Scalar(0, 0, 255), 3, Imgproc.LINE_AA, 0);
//        }

        //return the result bitmap
        Bitmap tempBitmap = bitmapOriginal.copy(Bitmap.Config.ARGB_8888, true);
        Utils.matToBitmap(dstMat, tempBitmap);

        return tempBitmap;
    }

    /**
     * adaptative threshold
     *
     * @param bitmapOriginal
     * @return
     */
    public static Bitmap adaptativeThreshold_ForBorder(Bitmap bitmapOriginal) {

        Mat srcMat = new Mat();
        Mat dstMat = new Mat();
        Utils.bitmapToMat(bitmapOriginal, srcMat);

        //convert to gray
        Imgproc.cvtColor(srcMat, dstMat, Imgproc.COLOR_BGR2GRAY);

        //blur
        Imgproc.medianBlur(dstMat, dstMat, 5);

        //thresh hold
        srcMat = dstMat;
        Imgproc.adaptiveThreshold(srcMat, dstMat, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                Imgproc.THRESH_BINARY_INV, 199, 2);//21,2
//        Imgproc.adaptiveThreshold(srcMat, dstMat, 60, 255, Imgproc.THRESH_BINARY);

        //return the result bitmap
        Bitmap resultBitmap = bitmapOriginal.copy(Bitmap.Config.ARGB_8888, true);
        Utils.matToBitmap(dstMat, resultBitmap);
        return resultBitmap;
    }

    /**
     * adaptative threshold 2
     * use bigger blur parameter
     *
     * @param bitmapOriginal
     * @return
     */
    public static Bitmap adaptativeThreshold_ForNormalizedImage(Bitmap bitmapOriginal) {

        Mat srcMat = new Mat();
        Mat dstMat = new Mat();
        Utils.bitmapToMat(bitmapOriginal, srcMat);

        //convert to gray
        Imgproc.cvtColor(srcMat, dstMat, Imgproc.COLOR_BGR2GRAY);

        //blur
        Imgproc.medianBlur(dstMat, dstMat, 9);
//        Imgproc.bilateralFilter(dstMat,dstMat,-1,20,20);

        //thresh hold
        srcMat = dstMat;
        Imgproc.adaptiveThreshold(srcMat, dstMat, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                Imgproc.THRESH_BINARY_INV, 21, 2);
//        Imgproc.adaptiveThreshold(srcMat, dstMat, 60, 255, Imgproc.THRESH_BINARY);

        //return the result bitmap
        Bitmap resultBitmap = bitmapOriginal.copy(Bitmap.Config.ARGB_8888, true);
        Utils.matToBitmap(dstMat, resultBitmap);
        return resultBitmap;
    }

    /**
     * adaptative threshold, don't inverse the color
     *
     * @param bitmapOriginal
     * @return
     */
    public static Bitmap adaptativeThreshold_ForCells(Bitmap bitmapOriginal) {

        Mat srcMat = new Mat();
        Mat dstMat = new Mat();
        Utils.bitmapToMat(bitmapOriginal, srcMat);

        //convert to gray
        Imgproc.cvtColor(srcMat, dstMat, Imgproc.COLOR_BGR2GRAY);

        //blur
        Imgproc.medianBlur(dstMat, dstMat, 11);
//        Imgproc.bilateralFilter(dstMat,dstMat,-1,20,20);

        //thresh hold
        srcMat = dstMat;
        Imgproc.adaptiveThreshold(srcMat, dstMat, 255, Imgproc.ADAPTIVE_THRESH_GAUSSIAN_C,
                Imgproc.THRESH_BINARY, 15, 2);
//        Imgproc.adaptiveThreshold(srcMat, dstMat, 60, 255, Imgproc.THRESH_BINARY);

        //return the result bitmap
        Bitmap resultBitmap = bitmapOriginal.copy(Bitmap.Config.ARGB_8888, true);
        Utils.matToBitmap(dstMat, resultBitmap);
        return resultBitmap;
    }

    /**
     * find Hough Lines
     */
    public Bitmap houghLines(Bitmap bitmap) {

        Mat srcMat = new Mat();
        Mat dstMat = new Mat();
        Utils.bitmapToMat(bitmap, srcMat);

        Imgproc.cvtColor(srcMat, dstMat, Imgproc.COLOR_GRAY2BGR);

        Mat lines = new Mat();

        Imgproc.HoughLinesP(srcMat, lines, 1, Math.PI / 180, 50, 1000, 300);
        Log.i(TAG, "lines: " + lines.size());
        // Draw the lines
        for (int x = 0; x < lines.rows(); x++) {
            double[] l = lines.get(x, 0);
            Imgproc.line(dstMat, new Point(l[0], l[1]), new Point(l[2], l[3]),
                    new Scalar(0, 0, 255), 3, Imgproc.LINE_AA, 0);
        }

        //return the result bitmap
        Bitmap resultBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Utils.matToBitmap(dstMat, resultBitmap);

        return resultBitmap;
    }

    /**
     * find Contours
     *
     * @param bitmap
     * @return
     */
    public MatOfPoint2f findMaxContours(Bitmap bitmap) {

        MatOfPoint2f maxPolygonMat = new MatOfPoint2f();

        Mat srcMat = new Mat();
        Mat dstMat = new Mat();
        Utils.bitmapToMat(bitmap, srcMat);
        //turn it to gray mode
        Imgproc.cvtColor(srcMat, srcMat, Imgproc.COLOR_BGR2GRAY);

        // http://answers.opencv.org/question/103377/android-findcontours-returning-too-many-contours/
        List<MatOfPoint> contours = new ArrayList<>();
        Mat hierarchy = new Mat(srcMat.size(), CvType.CV_8UC1);
        Imgproc.findContours(srcMat, contours, hierarchy, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

        // find the best candidate
        Log.i("::find contour::", "contours number " + contours.size());
        double maxArea = 0.0;


        for (int i = 0; i < contours.size(); i++) {
            MatOfPoint contour = contours.get(i);
            MatOfPoint2f contour2f = new MatOfPoint2f();
            MatOfPoint2f approxCurve = new MatOfPoint2f();

            contour.convertTo(contour2f, CvType.CV_32FC2);
            // old method to convert: contour2f.fromList(contour.toList());

            double approxDistance = Imgproc.arcLength(contour2f, true) * 0.02;
            Imgproc.approxPolyDP(contour2f, approxCurve, approxDistance, true);

            //has the polygon 4 sides?
            if (approxCurve.toArray().length != 4) {//only quadrilateral
                continue;
            }

            //is the polygon convex ?
            MatOfPoint polyMatOfPoint = new MatOfPoint();
            approxCurve.convertTo(polyMatOfPoint, CvType.CV_32S);
            if (!Imgproc.isContourConvex(polyMatOfPoint)) {
                continue;
            }

            double contourArea = Imgproc.contourArea(approxCurve);
            if (maxArea < contourArea) {
                maxArea = contourArea;
                maxPolygonMat = approxCurve;
            }
        }

//        approxCurve = maxContour;

        //draw result, change to color mode
        Imgproc.cvtColor(srcMat, srcMat, Imgproc.COLOR_GRAY2BGR);

        //outline the max shape
        int lineNum = maxPolygonMat.toArray().length;
        for (int i = 0; i < maxPolygonMat.toArray().length; i++) {
            Point pt1 = maxPolygonMat.toArray()[i % lineNum];
            Point pt2 = maxPolygonMat.toArray()[(i + 1) % lineNum];
//            Point pt1 = new Point(pt1Mat.get(0,0)[0], pt1Mat.get(0,1)[1]);
//            Point pt2 = new Point(pt2Mat.get(0,0)[0], pt2Mat.get(0,1)[1]);
            Imgproc.line(srcMat, pt1, pt2, new Scalar(0, 255, 0), 7);
        }

        //draw them, firstly turn the mat to color mode
        Imgproc.drawContours(srcMat, contours, -1, new Scalar(0, 0, 255));

        dstMat = srcMat;

        //return the result bitmap
//        Bitmap resultBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
//        Utils.matToBitmap(dstMat, resultBitmap);
//
//        return resultBitmap;

        return maxPolygonMat;
    }

    /**
     * Transform perspective to a formal square
     * Only need square shape(normalized)
     * @param bitmap
     */
    public Bitmap transformPerspective(Bitmap bitmapOriginal, MatOfPoint2f maxPolygonMat) {

        Mat dstMat = new Mat();

        Mat srcMat = Mat.zeros(4, 2, CvType.CV_32F);

        srcMat.put(0, 0, 0);
        srcMat.put(0, 1, NORMAL_IMAGE_SIZE);
        srcMat.put(1, 0, 0);
        srcMat.put(1, 1, 0);
        srcMat.put(2, 0, NORMAL_IMAGE_SIZE);
        srcMat.put(2, 1, 0);
        srcMat.put(3, 0, NORMAL_IMAGE_SIZE);
        srcMat.put(3, 1, NORMAL_IMAGE_SIZE);


        Mat grid = orderPoints(maxPolygonMat);

        srcMat = Imgproc.getPerspectiveTransform(grid, srcMat);

        Mat originalMat = new Mat();

        Utils.bitmapToMat(bitmapOriginal, originalMat);

        Imgproc.warpPerspective(originalMat, dstMat, srcMat, new Size(NORMAL_IMAGE_SIZE, NORMAL_IMAGE_SIZE));

        //return the result bitmap
        Bitmap bitmapNormalized = Bitmap.createBitmap(dstMat.cols(), dstMat.rows(), Bitmap.Config.ARGB_8888);
        Utils.matToBitmap(dstMat, bitmapNormalized);

        return bitmapNormalized;
    }

    /**
     * copy from the Internet, don't know why it works
     *
     * @param pts
     * @return
     */
    private static Mat orderPoints(Mat pts) {
        Mat rect = Mat.zeros(4, 2, CvType.CV_32F);
        Mat s = Mat.zeros(1, 4, CvType.CV_32F);
        Mat diff = Mat.zeros(1, 4, CvType.CV_32F);
        for (int i = 0; i < s.width(); i++) {
            s.put(0, i, pts.get(i, 0)[1] + pts.get(i, 0)[0]);
        }
        double min = pts.get(0, 0)[1] + pts.get(0, 0)[0];
        double max = pts.get(0, 0)[1] + pts.get(0, 0)[0];
        int id_min = 0, id_max = 0;
        for (int i = 0; i < s.width(); i++) {
            double val = pts.get(i, 0)[1] + pts.get(i, 0)[0];
            if (val <= min) {
                min = val;
                id_min = i;
            }
            if (val > max) {
                max = val;
                id_max = i;
            }
        }

        rect.put(1, 0, pts.get(id_min, 0)[0]);
        rect.put(1, 1, pts.get(id_min, 0)[1]);
        rect.put(3, 0, pts.get(id_max, 0)[0]);
        rect.put(3, 1, pts.get(id_max, 0)[1]);

        for (int i = 0; i < diff.width(); i++) {
            diff.put(0, i, pts.get(i, 0)[1] - pts.get(i, 0)[0]);
        }
        min = pts.get(0, 0)[1] - pts.get(0, 0)[0];
        max = pts.get(0, 0)[1] - pts.get(0, 0)[0];
        id_min = 0;
        id_max = 0;
        for (int i = 0; i < diff.width(); i++) {
            double val = pts.get(i, 0)[1] - pts.get(i, 0)[0];
            if (val <= min) {
                min = val;
                id_min = i;
            }
            if (val > max) {
                max = val;
                id_max = i;
            }
        }

        rect.put(2, 0, pts.get(id_min, 0)[0]);
        rect.put(2, 1, pts.get(id_min, 0)[1]);
        rect.put(0, 0, pts.get(id_max, 0)[0]);
        rect.put(0, 1, pts.get(id_max, 0)[1]);
        return rect;
    }

    /**
     * canny
     *
     */
    public Bitmap canny(Bitmap bitmap) {

        Mat srcMat = new Mat();
//        Mat dstMat = new Mat();
        Utils.bitmapToMat(bitmap, srcMat);

        Mat edges = new Mat(srcMat.size(), CvType.CV_8UC1);
        Imgproc.Canny(srcMat, edges, 50, 200, 3, false);
//        srcMat = edges;

        //return the result bitmap
        Bitmap resultBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true);
        Utils.matToBitmap(edges, resultBitmap);

        return resultBitmap;
    }

    /**
     * split the picture to 9X9 grids
     *
     * @param bitmap
     * @return List<Rect>
     */
    public List<Rect> splitCells(Bitmap bitmap) {

        List<Rect> numberRectList = new ArrayList<>();

        //threshold
        Bitmap bitmapTemp = adaptativeThreshold_ForNormalizedImage(bitmap);

        Mat srcMat = new Mat();
        Mat dstMat = new Mat();

//        dstMat = srcMat.clone();

        Mat stats = new Mat();
        Utils.bitmapToMat(bitmapTemp, srcMat);

        //convert to gray
        Imgproc.cvtColor(srcMat, srcMat, Imgproc.COLOR_BGR2GRAY);

        Mat labeled = new Mat(srcMat.size(), srcMat.type());
        Mat rectComponents = Mat.zeros(new Size(0, 0), 0);
        Mat centComponents = Mat.zeros(new Size(0, 0), 0);

        Imgproc.connectedComponentsWithStats(srcMat, labeled, rectComponents, centComponents, 8, CvType.CV_32S);

        int[] rectangleInfo = new int[5];
        double[] centroidInfo = new double[2];


        Imgproc.cvtColor(srcMat, srcMat, Imgproc.COLOR_GRAY2BGR);
        Log.i("::components number::", "size: " + rectComponents.rows());
        for (int i = 1; i < rectComponents.rows(); i++) {

            // Extract bounding box
            rectComponents.row(i).get(0, 0, rectangleInfo);
            Rect currentRect = new Rect(rectangleInfo[0], rectangleInfo[1], rectangleInfo[2], rectangleInfo[3]);

            centComponents.row(i).get(0, 0, centroidInfo);

            Point point1 = new Point(rectangleInfo[0], rectangleInfo[1]);
            Point point2 = new Point(rectangleInfo[0] + rectangleInfo[2], rectangleInfo[1] + rectangleInfo[3]);

            //filter the small and big areas, they are noise
            int rectArea = rectangleInfo[2] * rectangleInfo[3];
            if (rectArea < 300 || rectArea > 12000) {
                continue;
            }
            //filter the line gaps
            if (rectangleInfo[2] > 100
                    || rectangleInfo[3] > 100
                    || rectangleInfo[3] < 40) {
                continue;
            }

            Imgproc.rectangle(srcMat, point1, point2, new Scalar(0, 255, 0), 7);

            //add to the list for future use
            numberRectList.add(currentRect);
        }
//
        dstMat = srcMat;

        //return the result bitmap
//        Bitmap resultBitmap = Bitmap.createBitmap(dstMat.cols(), dstMat.rows(), Bitmap.Config.ARGB_8888);
//        Utils.matToBitmap(dstMat, resultBitmap);
//
//        return resultBitmap;
        return numberRectList;
    }

    /**
     * Scale the number image, in order to increase the recognition accuracy
     *
     * @param rect
     * @return
     */
    public Rect rectScale(Rect rect) {

        // 80% of the normal cell, 113*0.8 = 90
        int newSize = (int) (113 * 0.7);

        int centroidX = (int) (rect.tl().x + ((rect.br().x - rect.tl().x) / 2));
        int centroidY = (int) (rect.tl().y + ((rect.br().y - rect.tl().y) / 2));

        int tempX = centroidX - newSize / 2;
        if(tempX<0){
            tempX = 0;
        }
        if((tempX+newSize)>1017) { //1017
            tempX = 1017-newSize;
        }

        int tempY = centroidY - newSize / 2;
        if(tempY<0){
            tempY = 0;
        }
        if((tempY+newSize)>1017) { //1017
            tempY = 1017-newSize;
        }


        Rect newRect = new Rect(tempX, tempY,
                newSize, newSize);

        if (newRect.area() < rect.area()) {
            //don't need to Scale...
            return rect;
        }
        return newRect;
    }

    /**
     * if the centroid of the rect is not close to
     * the centroid of the cells, it is noise
     * @param rectList
     * @return
     */
    private List<Rect> filterRects(List<Rect> rectList) {
        //filter the small noise
        List<Rect> finalRectList = new ArrayList<>();
        for (int i = 0; i < rectList.size(); i++) {
            Rect tempRect = rectList.get(i);

            int rowNum = tempRect.y / 113  +1;
            int colNum = tempRect.x / 113  +1;

            int rectCentroidX = (int) (tempRect.tl().x + ((tempRect.br().x - tempRect.tl().x) / 2));
            int rectCentroidY = (int) (tempRect.tl().y + ((tempRect.br().y - tempRect.tl().y) / 2));

            int cellCentroidX = (int) (113 / 2 + 113 * (colNum - 1));
            int cellCentroidY = (int) (113 / 2 + 113 * (rowNum - 1));

            double dist =
                    Math.sqrt(Double.valueOf((cellCentroidX - rectCentroidX) * (cellCentroidX - rectCentroidX)
                            + (cellCentroidY - rectCentroidY) * (cellCentroidY - rectCentroidY)));

            if(dist<30.0){
                finalRectList.add(tempRect);
            }
        }
        return finalRectList;
    }
}
