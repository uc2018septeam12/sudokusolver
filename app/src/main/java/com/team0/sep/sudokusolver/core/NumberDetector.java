package com.team0.sep.sudokusolver.core;

import android.graphics.Bitmap;
import android.os.Environment;
import android.support.annotation.NonNull;

import com.googlecode.tesseract.android.TessBaseAPI;
import com.team0.sep.sudokusolver.R;

import java.io.File;
import java.io.InputStream;

public class NumberDetector {

    private static TessBaseAPI tessBaseAPI = new TessBaseAPI();

    public final static String TRAINED_DATA_FOLDER = "/Sudoku_tesseract";

    public final static String TRAINED_DATA_NAME = "num.traineddata";

    /**
     * constructor
     */
    public NumberDetector(){
        initTessBaseAPI();
    }

    /**
     * initialize the Tesseract API
     */
    public void initTessBaseAPI() {

        String dataPath = Environment.getExternalStorageDirectory() + TRAINED_DATA_FOLDER; //训练数据路径

        tessBaseAPI.setDebug(true);
//        tessBaseAPI.init(dataPath, "eng"); //eng为识别语言
        tessBaseAPI.init(dataPath, "num"); //num为识别语言, 对应的语言包是num.traineddata
//        tessBaseAPI.setVariable(TessBaseAPI.VAR_CHAR_WHITELIST, "123456789"); // 识别白名单
//        tessBaseAPI.setVariable(TessBaseAPI.VAR_CHAR_BLACKLIST, "!@#$%^&*()_+=-qwertyuiop[]}{POIU" +
//                "YTREWQasdASDfghFGHjklJKLl;L:'\"\\|~`xcvXCVbnmBNM,./<>?"); // 识别黑名单
        tessBaseAPI.setPageSegMode(TessBaseAPI.PageSegMode.PSM_SINGLE_CHAR);//设置识别模式
    }


    /**
     * OCR interpret
     */
    public String detectText(Bitmap bitmap) {

        tessBaseAPI.setImage(bitmap); //设置需要识别图片的bitmap
//        String inspection = tessBaseAPI.getHOCRText(0);
        String inspection = tessBaseAPI.getUTF8Text();
//        tessBaseAPI.end();
        return inspection;
    }
}
