package com.team0.sep.sudokusolver.core;

/**
 * Provide reasonable hint to user
 * from Dimon
 */
public class SudokuHintSolver {

    /**
     * @param sudokuHintMatrix  the last hint matrix
     * @param sudokuAllResultMatrix  the full solutions of the Sudoku
     * @return
     */
    public static int[][] solveStepByStep(int[][] sudokuHintMatrix, int[][] sudokuAllResultMatrix) {

        int[][] sodukuResultMatrix = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                sodukuResultMatrix[i][j] = sudokuHintMatrix[i][j];
            }
        }

        /**
         * check which is the unsolved number
         * from 1 to 9
         */
        int unSolvedNum = 1;
        boolean isFullFilled = false;
        for (int num = 1; num <= 9; num++) {
            boolean hasThisNumberInAllRows = true;
            for (int rowNum = 0; rowNum < 9; rowNum++) {
                boolean hasThisNumInRow = false;
                currentRow:
                for (int colNum = 0; colNum < 9; colNum++) {
                    if (sudokuHintMatrix[rowNum][colNum] == num) {
                        hasThisNumInRow = true;
                        break currentRow;
                    }
                }
                if (!hasThisNumInRow) {
                    hasThisNumberInAllRows = false;
                }
            }
            //find the unSolved number
            if (!hasThisNumberInAllRows) {
                unSolvedNum = num;
                break;
            }
            //all numbers are solved
            if (num == 9) {
                isFullFilled = true;
            }
        }

        // if all numbers are solved, return
        if (isFullFilled) {
            sodukuResultMatrix = sudokuAllResultMatrix;
            return sodukuResultMatrix;
        }

        // only add the unfilled numbers to the hint matrix
        for (int rowNum = 0; rowNum < 9; rowNum++) {
            for (int colNum = 0; colNum < 9; colNum++) {
                if (sudokuHintMatrix[rowNum][colNum] == 0
                        && sudokuAllResultMatrix[rowNum][colNum] == unSolvedNum) {
                    sodukuResultMatrix[rowNum][colNum] = unSolvedNum;
                }
            }
        }

        return sodukuResultMatrix;
    }


    public static int[][] solveAll(int[][] sodukuHintMatrix) {
        int[][] data = init(sodukuHintMatrix);

        int[][] correctAnswer = new int[9][9];

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (data[i][j] == 0) {
                    data[i][j] = 0x1ff;
                }
            }
        }
        int[][] data1 = copyData(data);


        solve(data, data1, correctAnswer);

        return correctAnswer;
    }

    private static void solve(int[][] data, int[][] data1, int[][] correctAnswer) {
        analyse(data);
        int result = check(data);
        if (result == 1) {
            int[] position = smallPosition(data);
            int pv = data[position[0]][position[1]];
            int pvcount = Integer.bitCount(pv);
            for (int i = 0; i < pvcount; i++) {
                int testV = 1 << ((int) (Math.log(Integer.highestOneBit(pv)) / Math.log(2)));
                pv ^= testV;
                int[][] copy = copyData(data);
                copy[position[0]][position[1]] = testV;
                solve(copy, data1, correctAnswer);
            }
        } else if (result == 0) {

            print(data, data1, correctAnswer);

        }
    }


    private static int[][] copyData(int[][] data) {
        int[][] copy = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                copy[i][j] = data[i][j];

            }

        }

        return copy;
    }


    public static int[] smallPosition(int[][] data) {
        int[] res = null;
        int smallCount = 10;
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                int bitcount = Integer.bitCount(data[i][j]);
                if (bitcount == 2) {
                    return new int[]{i, j};
                } else if (bitcount != 1) {
                    if (smallCount > bitcount) {
                        smallCount = bitcount;
                        res = new int[]{i, j};
                    }
                }
            }
        }
        return res;
    }


    private static int check(int[][] data) {
        for (int i = 0; i < 9; i++) {
            int row = 0;
            int col = 0;
            int block = 0;
            for (int j = 0; j < 9; j++) {
                if (Integer.bitCount(data[i][j]) > 1) {
                    return 1;
                }
                row |= data[i][j];
                col |= data[j][i];
            }

            for (int h = i / 3 * 3; h < i / 3 * 3 + 3; h++) {
                for (int l = i % 3 * 3; l < i % 3 * 3 + 3; l++) {
                    block |= data[h][l];
                }
            }
            if (row != 0x1ff || col != 0x1ff || block != 0x1ff) {
                return -1;
            }
        }
        return 0;
    }

    private static void analyse(int[][] data) {
        boolean changed = false;
        changed = reduce(data);

        if (changed) {
            analyse(data);
        }
    }

    private static boolean reduce(int[][] data) {
        boolean changed = false;
        for (int m = 0; m < 9; m++) {
            for (int n = 0; n < 9; n++) {
                if (Integer.bitCount(data[m][n]) != 1) {
                    if (setMaybe(data, m, n)) {
                        changed = true;
                    }
                }
            }
        }
        return changed;
    }

    private static boolean setMaybe(int[][] data, int m, int n) {
        if (Integer.bitCount(data[m][n]) == 1) {
            return false;
        }
        int row = 0;
        int col = 0;
        int block = 0;

        for (int i = 0; i < 9; i++) {
            if (Integer.bitCount(data[m][i]) == 1) {
                row += data[m][i];
            }
            if (Integer.bitCount(data[i][n]) == 1) {
                col += data[i][n];
            }
        }

        for (int i = m / 3 * 3; i < m / 3 * 3 + 3; i++) {
            for (int j = n / 3 * 3; j < n / 3 * 3 + 3; j++) {
                if (Integer.bitCount(data[i][j]) == 1) {
                    block += data[i][j];
                }
            }
        }

        int have = row | col | block;// 不可能的值
        int left = 0x1ff ^ have;// 候选数
        // System.out.println("["+m+","+n+"]"+Integer.toBinaryString(left));
        return tryReduce(data, m, n, left);
    }

    private static boolean tryReduce(int[][] data, int m, int n, int v) {
        int old = data[m][n];
        data[m][n] = old & v;
        return data[m][n] != old;
    }

    private static int[][] init(int[][] source) {
        int[][] data = new int[9][9];
        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9; j++) {
                if (source[i][j] != 0) {
                    data[i][j] = 1 << (source[i][j] - 1);
                }
            }

        }
        return data;
    }

    private static void print(int[][] data, int[][] data1, int[][] correctAnswer) {
        // printhint(data1);   debug
        for (int m = 0; m < 9; m++) {
            for (int n = 0; n < 9; n++) {
                int v = getV9(data[m][n]);
                correctAnswer[m][n] = v;
                if (v != -1) {
                    // System.out.print(v + " ");
                    if (v == 2) {
                        data1[m][n] = v;
                    }

                } else {
                    //  System.out.print("_ ");
                }
            }
            // System.out.println();
        }
    }

    private static void print1(int[][] data) {
        for (int m = 0; m < 9; m++) {
            for (int n = 0; n < 9; n++) {
                int v = getV9(data[m][n]);
                if (v != -1) {
                    System.out.print(v + " ");
                } else {
                    System.out.print("_ ");
                }
            }
            System.out.println();
        }
    }

    private static void hint(int[][] data, int[][] data1) {
        for (int m = 0; m < 9; m++) {
            for (int n = 0; n < 9; n++) {
                int v = getV9(data[m][n]);
                //if (v != -1) {
                // System.out.print(v + " ");
                if (v == 1) {
                    data1[m][n] = v;
                }
                //  } else {
                // System.out.print("_ ");
                // }
            }
            //System.out.println();
        }

    }

    public static int getV9(int v) {
        switch (v) {
            case 1:
                return 1;
            case 2:
                return 2;
            case 4:
                return 3;
            case 8:
                return 4;
            case 16:
                return 5;
            case 32:
                return 6;
            case 64:
                return 7;
            case 128:
                return 8;
            case 256:
                return 9;
            default:
                break;
        }
        return -1;
    }

    private static void printAnswer(int[][] answer) {
        for (int m = 0; m < 9; m++) {
            for (int n = 0; n < 9; n++) {
                System.out.print(answer[m][n] + " ");

            }
            System.out.println();
        }
    }

}
