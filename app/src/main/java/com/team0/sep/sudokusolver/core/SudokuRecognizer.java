package com.team0.sep.sudokusolver.core;

import android.graphics.Bitmap;

import org.opencv.core.MatOfPoint2f;
import org.opencv.core.Rect;

import java.util.ArrayList;
import java.util.List;

public class SudokuRecognizer {

    private NumberDetector numberDetector;
    private ImageProcessor imageProcessor;

    private Bitmap bitmapOriginal; // original bitmap
    private Bitmap bitmapTemplate;
    private Bitmap bitmapNormalized; // the bitmap that is normalized

    // max polygon, supposed to be a rectangle
    private MatOfPoint2f maxPolygonMat;

    // the rects that have numbers inside the small image
    List<Rect> numberRectList = new ArrayList<>();

    int[][] sudokuNumMatrix = new int[9][9];

    public SudokuRecognizer(){
        //initialize the OpenCV/OCR api
        numberDetector = new NumberDetector();
        imageProcessor = new ImageProcessor();
    }


    public void setOriginalBitmap(Bitmap bitmap){
        this.bitmapOriginal = bitmap;
    }

    /**
     *  clean all the current state data
      */
    public void flush(){
        this.bitmapOriginal = null;
        this.bitmapNormalized = null;
        this.maxPolygonMat = new MatOfPoint2f();
        this.numberRectList = new ArrayList<>();
    }

    /**
     *  main method for sudoku image parsing.
     * @return
     */
    public int[][] processSudokuImage(){

        // find the biggest square
        maxPolygonMat =
                imageProcessor.findMaxContours(imageProcessor.adaptativeThreshold_ForBorder(bitmapOriginal));

        // normalize the square
        bitmapNormalized = imageProcessor.transformPerspective(bitmapOriginal,maxPolygonMat);

        // split the cells
        numberRectList = imageProcessor.splitCells(bitmapNormalized);

        //filter the small Rects(as noises)
        numberRectList = filterRects(numberRectList);

        // parse the cells, to numbers
        matchNumbers(numberRectList);

        return this.sudokuNumMatrix;

    }

    /**
     * try to recognize the number from the small rect of the original image
     * Using the OCR functions
     * @param numberRectList
     */
    public void matchNumbers(List<Rect> numberRectList) {

        for (Rect rect : numberRectList) {
//            Bitmap tempBitmap = Bitmap.createBitmap(bitmapNormalized,
//                    (int)(rect.x*0.8),(int)(rect.y*0.8),
//                    (int)(rect.width*(1.2)),(int)(rect.height*(1.2)));
            Rect newRect = imageProcessor.rectScale(rect);

            Bitmap tempBitmap = Bitmap.createBitmap(bitmapNormalized,
                    newRect.x, newRect.y,
                    newRect.width, newRect.height);

            tempBitmap = imageProcessor.adaptativeThreshold_ForCells(tempBitmap);

            String strResult = numberDetector.detectText(tempBitmap);

            int rowNum = rect.y / 113;
            int colNum = rect.x / 113;
            if (!strResult.equals("")) {
                sudokuNumMatrix[rowNum][colNum] = Integer.parseInt(strResult);
            }
        }
    }
}
